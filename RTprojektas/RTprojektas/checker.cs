﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace RTprojektas
{
    public partial class Checker : Form
    {
        public Checker()
        {
            InitializeComponent();
            tbChecker.MaxLength = 16;
        }
        

        private void tbChecker_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

       
        
        private void lblCheck_Click(object sender, EventArgs e)
        {
           

        }

        private void tbChecker_TextChanged(object sender, EventArgs e)
        {
            //Regex validator = new Regex("^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14})$");
            //string match = validator.Match(tbChecker.Text).Value.ToString();
            //if (match.Length == 16)
            //{
            //    lblCheck.Text = "Card is valid, ";
            //}
            //else
            //{
            //    lblCheck.Text = "Card is invalid";
            //}
            //////////////////////////////////////////////////////////////////////

            // Regex validator2 = new Regex("^4[0-9]{12}(?:[0-9]{3})?$");
            // string match2 = validator2.Match(tbChecker.Text).Value.ToString();
            // if (match2.Length == 16)
            //{
            //    lblCheck.Text = "Card is valid. Credit card type: Visa";
            // }
            // else
            // {
            //     lblCheck.Text = "Card is invalid";
            //  }
            checkCard();


        }

        void checkCard()
        {
            
            Regex validator2 = new Regex("^4[0-9]{12}(?:[0-9 ]{3})?$");
            string match2 = validator2.Match(tbChecker.Text).Value.ToString();
            Regex validator3 = new Regex("^(?:5[1-5][0-9]{2}|222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9]{12}$");
            string match3 = validator3.Match(tbChecker.Text).Value.ToString();
            Regex validator4 = new Regex("^(62[0-9]{14,17})$");
            string match4 = validator4.Match(tbChecker.Text).Value.ToString();
            
            if (radioButton1.Checked && match2.Length == 16)
            {
                lblCheck.Text = "Card is valid. Credit card type: Visa";
            }
            else if (radioButton3.Checked && match3.Length == 16)
            {
                lblCheck.Text = "Card is valid. Credit card type: Mastercard";
            }
            else if (radioButton4.Checked && match4.Length == 16)  
            {
                lblCheck.Text = "Card is valid. Credit card type: Union Pay ";
            }
            else
            {
                lblCheck.Text = "Card is invalid";
            }
        }

        private void Checker_Load(object sender, EventArgs e)
        {

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if(radioButton1.Checked)
            {
                checkCard();
            }
            

        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton3.Checked)
            {
                checkCard();
            }
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton4.Checked)
            {
                checkCard();
            }
        }

       }
    }

