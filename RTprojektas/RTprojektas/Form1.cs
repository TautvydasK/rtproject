﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Security.Cryptography;
using System.Collections;

namespace RTprojektas
{



    public partial class Form1 : Form 
    {

        Hashtable ht = new Hashtable();
        string str;
        public Form1()
        {

            InitializeComponent();
        }


        private void btnLogin_Click(object sender, EventArgs e)
        {
            
            MessageBox.Show("Sekmingai prisijungete!");


            string hashPassFromDB = (string)ht[tbUser1.Text];
            

            //string line = File.ReadLines("C:\\Users\\Admin\\Desktop\\rtproject\\RTprojektas\\usernames.txt").ElementAt();
            string hashedPass = CryptoHelper.MD5String(tbPass1.Text);

            
        
            if (hashedPass != hashPassFromDB)
            {
                MessageBox.Show("Slaptazodis neteisingas");
                return;
            }
            

            Checker chk = new Checker();
            chk.ShowDialog();

        }
        private void btnReg_Click(object sender, EventArgs e)
        {
          if (ht.Contains(tbUser.Text))
            {
                MessageBox.Show("Toks vartotojas jau yra");
                return;
            }
            string text = tbUser.Text;
            TextWriter txt = new StreamWriter("C:\\Users\\Admin\\Desktop\\rtproject\\RTprojektas\\usernames.txt", true);
            //txt.Write(tbUser.Text + " " + tbPass.Text + Environment.NewLine);

            string hashedPass = CryptoHelper.MD5String( tbPass.Text);
            
           

            txt.Write(tbUser.Text + Environment.NewLine + hashedPass + Environment.NewLine);
            txt.Close();
            MessageBox.Show("Prisiregistruota");

            
           
               ht.Add(tbUser.Text, hashedPass);
            

            
          
            

        }

        private void tbUser1_TextChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string[] fileLines = File.ReadAllLines(@"C:\\Users\\Admin\\Desktop\\rtproject\\RTprojektas\\usernames.txt");

            if (fileLines.Length % 2 == 1 || fileLines.Length < 1)
            {
              
                return;
            }
            
            for (int i = 0; i < fileLines.Length; i+=2)
            {
                ht.Add(fileLines[i], fileLines[i + 1]);
            }
        }

        private void tbPass_TextChanged(object sender, EventArgs e)
        {

        }

        public static string GetMD5Hash(string str)
        {
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            byte[] bytes = ASCIIEncoding.Default.GetBytes(str);
            byte[] encoded = md5.ComputeHash(bytes);

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < encoded.Length; i++)
                sb.Append(encoded[i].ToString("x2"));

            return sb.ToString();
        }

        public static string GetMD5Hash2(string str)
        {
            System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] bytes = Encoding.Default.GetBytes(str);
            byte[] encoded = md5.ComputeHash(bytes);

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < encoded.Length; i++)
                sb.Append(encoded[i].ToString("x2"));

            return sb.ToString();
        }

        private void tbUser_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar==13)
            {
                tbPass.Focus();
            }
        }
    }
    }


