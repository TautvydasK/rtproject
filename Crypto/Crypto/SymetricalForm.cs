﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Crypto
{
    public partial class SymetricalForm : Form
    {
        public SymetricalForm()
        {
            InitializeComponent();
        }

        private void btnBrowseFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();

            if(ofd.ShowDialog() == DialogResult.OK)
            {
                txtFile.Text = ofd.FileName;
            }
        }

        private void btnBrowseSaveTo_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();

            if(sfd.ShowDialog() == DialogResult.OK)
            {
                txtSaveTo.Text = sfd.FileName;
            }
        }

        private void btnTextEncode_Click(object sender, EventArgs e)
        {
            if(txtKey.Text == "" || txtText.Text == "")
            {
                return;
            }
        }

        private void btnTextDecode_Click(object sender, EventArgs e)
        {
            if (txtKey.Text == "" || txtText.Text == "")
            {
                return;
            }
        }

        private void btnFileEncode_Click(object sender, EventArgs e)
        {
            if(!File.Exists(txtFile.Text) || txtKey.Text == "")
            {
                return;
            }
        }

        private void btnFileDecode_Click(object sender, EventArgs e)
        {
            if (!File.Exists(txtFile.Text) || txtKey.Text == "")
            {
                return;
            }
        }
    }
}
