﻿namespace Crypto
{
    partial class SymetricalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtText = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtKey = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtFile = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtSaveTo = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.btnTextEncode = new System.Windows.Forms.Button();
            this.btnTextDecode = new System.Windows.Forms.Button();
            this.btnFileDecode = new System.Windows.Forms.Button();
            this.btnFileEncode = new System.Windows.Forms.Button();
            this.btnBrowseFile = new System.Windows.Forms.Button();
            this.btnBrowseSaveTo = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtText
            // 
            this.txtText.Location = new System.Drawing.Point(65, 19);
            this.txtText.Name = "txtText";
            this.txtText.Size = new System.Drawing.Size(363, 20);
            this.txtText.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Text:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnTextDecode);
            this.groupBox1.Controls.Add(this.btnTextEncode);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.textBox6);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.textBox4);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtText);
            this.groupBox1.Location = new System.Drawing.Point(12, 38);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(447, 129);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Text";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnBrowseSaveTo);
            this.groupBox2.Controls.Add(this.btnBrowseFile);
            this.groupBox2.Controls.Add(this.btnFileDecode);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.btnFileEncode);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.txtSaveTo);
            this.groupBox2.Controls.Add(this.txtFile);
            this.groupBox2.Location = new System.Drawing.Point(12, 173);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(447, 102);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Files";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(28, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Key:";
            // 
            // txtKey
            // 
            this.txtKey.Location = new System.Drawing.Point(59, 12);
            this.txtKey.Name = "txtKey";
            this.txtKey.Size = new System.Drawing.Size(381, 20);
            this.txtKey.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(26, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "File:";
            // 
            // txtFile
            // 
            this.txtFile.Location = new System.Drawing.Point(65, 19);
            this.txtFile.Name = "txtFile";
            this.txtFile.ReadOnly = true;
            this.txtFile.Size = new System.Drawing.Size(282, 20);
            this.txtFile.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Encoded:";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(65, 45);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(363, 20);
            this.textBox4.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 48);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Save to:";
            // 
            // txtSaveTo
            // 
            this.txtSaveTo.Location = new System.Drawing.Point(65, 45);
            this.txtSaveTo.Name = "txtSaveTo";
            this.txtSaveTo.ReadOnly = true;
            this.txtSaveTo.Size = new System.Drawing.Size(282, 20);
            this.txtSaveTo.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 74);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Decoded:";
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(65, 71);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(363, 20);
            this.textBox6.TabIndex = 7;
            // 
            // btnTextEncode
            // 
            this.btnTextEncode.Location = new System.Drawing.Point(353, 97);
            this.btnTextEncode.Name = "btnTextEncode";
            this.btnTextEncode.Size = new System.Drawing.Size(75, 23);
            this.btnTextEncode.TabIndex = 8;
            this.btnTextEncode.Text = "Encode";
            this.btnTextEncode.UseVisualStyleBackColor = true;
            this.btnTextEncode.Click += new System.EventHandler(this.btnTextEncode_Click);
            // 
            // btnTextDecode
            // 
            this.btnTextDecode.Location = new System.Drawing.Point(272, 97);
            this.btnTextDecode.Name = "btnTextDecode";
            this.btnTextDecode.Size = new System.Drawing.Size(75, 23);
            this.btnTextDecode.TabIndex = 9;
            this.btnTextDecode.Text = "Decode";
            this.btnTextDecode.UseVisualStyleBackColor = true;
            this.btnTextDecode.Click += new System.EventHandler(this.btnTextDecode_Click);
            // 
            // btnFileDecode
            // 
            this.btnFileDecode.Location = new System.Drawing.Point(272, 71);
            this.btnFileDecode.Name = "btnFileDecode";
            this.btnFileDecode.Size = new System.Drawing.Size(75, 23);
            this.btnFileDecode.TabIndex = 11;
            this.btnFileDecode.Text = "Decode";
            this.btnFileDecode.UseVisualStyleBackColor = true;
            this.btnFileDecode.Click += new System.EventHandler(this.btnFileDecode_Click);
            // 
            // btnFileEncode
            // 
            this.btnFileEncode.Location = new System.Drawing.Point(353, 71);
            this.btnFileEncode.Name = "btnFileEncode";
            this.btnFileEncode.Size = new System.Drawing.Size(75, 23);
            this.btnFileEncode.TabIndex = 10;
            this.btnFileEncode.Text = "Encode";
            this.btnFileEncode.UseVisualStyleBackColor = true;
            this.btnFileEncode.Click += new System.EventHandler(this.btnFileEncode_Click);
            // 
            // btnBrowseFile
            // 
            this.btnBrowseFile.Location = new System.Drawing.Point(353, 17);
            this.btnBrowseFile.Name = "btnBrowseFile";
            this.btnBrowseFile.Size = new System.Drawing.Size(75, 23);
            this.btnBrowseFile.TabIndex = 12;
            this.btnBrowseFile.Text = "Browse";
            this.btnBrowseFile.UseVisualStyleBackColor = true;
            this.btnBrowseFile.Click += new System.EventHandler(this.btnBrowseFile_Click);
            // 
            // btnBrowseSaveTo
            // 
            this.btnBrowseSaveTo.Location = new System.Drawing.Point(353, 43);
            this.btnBrowseSaveTo.Name = "btnBrowseSaveTo";
            this.btnBrowseSaveTo.Size = new System.Drawing.Size(75, 23);
            this.btnBrowseSaveTo.TabIndex = 13;
            this.btnBrowseSaveTo.Text = "Browse";
            this.btnBrowseSaveTo.UseVisualStyleBackColor = true;
            this.btnBrowseSaveTo.Click += new System.EventHandler(this.btnBrowseSaveTo_Click);
            // 
            // SymetricalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(471, 282);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtKey);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "SymetricalForm";
            this.Text = "SymetricalForm";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtText;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnTextDecode;
        private System.Windows.Forms.Button btnTextEncode;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnBrowseSaveTo;
        private System.Windows.Forms.Button btnBrowseFile;
        private System.Windows.Forms.Button btnFileDecode;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnFileEncode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtSaveTo;
        private System.Windows.Forms.TextBox txtFile;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtKey;
    }
}