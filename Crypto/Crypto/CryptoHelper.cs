﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.IO;

namespace Crypto
{
    class CryptoHelper
    {
        #region Hash

        public static string MD5String(string inText)
        {
            //Convert string to byte array
            byte[] stringBytes = Encoding.UTF8.GetBytes(inText);

            MD5CryptoServiceProvider encoder = new MD5CryptoServiceProvider();

            byte[] hash = encoder.ComputeHash(stringBytes);

            StringBuilder outText = new StringBuilder();

            foreach (byte item in hash)
            {
                outText.Append(item.ToString("x2"));
            }

            return outText.ToString();
        }

        public static string MD5File(string path)
        {
            //Convert file in path to byte array
            byte[] bytes = File.ReadAllBytes(path);
            
            MD5CryptoServiceProvider encoder = new MD5CryptoServiceProvider();

            //computing hash for bytes array
            byte[] hash = encoder.ComputeHash(bytes);

            StringBuilder outText = new StringBuilder();

            foreach (byte item in hash)
            {
                outText.Append(item.ToString("x2"));
            }

            return outText.ToString().ToUpper();
        }

        public static string SHA256File(string path)
        {
            //Convert file to byte array
            byte[] bytes = File.ReadAllBytes(path);

            SHA256CryptoServiceProvider crypto = new SHA256CryptoServiceProvider();

            //encoding bytes
            byte[] hash = crypto.ComputeHash(bytes);

            StringBuilder outText = new StringBuilder();

            foreach (byte item in hash)
            {
                outText.Append(item.ToString("x2"));
            }

            return outText.ToString().ToUpper();
        }

        #endregion
    }
}
