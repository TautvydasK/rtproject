﻿namespace Crypto
{
    partial class HashForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtText = new System.Windows.Forms.TextBox();
            this.txtMD5Hash = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnMD5 = new System.Windows.Forms.Button();
            this.txtMD5File = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnMD5Browse = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnSHABrowse = new System.Windows.Forms.Button();
            this.txtShaFile = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtSHA256Hash = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnMD5Browse);
            this.groupBox1.Controls.Add(this.txtMD5File);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.btnMD5);
            this.groupBox1.Controls.Add(this.txtMD5Hash);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtText);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(467, 103);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "MD5";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Text:";
            // 
            // txtText
            // 
            this.txtText.Location = new System.Drawing.Point(69, 23);
            this.txtText.Name = "txtText";
            this.txtText.Size = new System.Drawing.Size(311, 20);
            this.txtText.TabIndex = 1;
            // 
            // txtMD5Hash
            // 
            this.txtMD5Hash.Location = new System.Drawing.Point(69, 75);
            this.txtMD5Hash.Name = "txtMD5Hash";
            this.txtMD5Hash.ReadOnly = true;
            this.txtMD5Hash.Size = new System.Drawing.Size(392, 20);
            this.txtMD5Hash.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Hash:";
            // 
            // btnMD5
            // 
            this.btnMD5.Location = new System.Drawing.Point(386, 21);
            this.btnMD5.Name = "btnMD5";
            this.btnMD5.Size = new System.Drawing.Size(75, 23);
            this.btnMD5.TabIndex = 4;
            this.btnMD5.Text = "Hash";
            this.btnMD5.UseVisualStyleBackColor = true;
            this.btnMD5.Click += new System.EventHandler(this.btnMD5_Click);
            // 
            // txtMD5File
            // 
            this.txtMD5File.Location = new System.Drawing.Point(69, 49);
            this.txtMD5File.Name = "txtMD5File";
            this.txtMD5File.ReadOnly = true;
            this.txtMD5File.Size = new System.Drawing.Size(311, 20);
            this.txtMD5File.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(28, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(26, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "File:";
            // 
            // btnMD5Browse
            // 
            this.btnMD5Browse.Location = new System.Drawing.Point(386, 47);
            this.btnMD5Browse.Name = "btnMD5Browse";
            this.btnMD5Browse.Size = new System.Drawing.Size(75, 23);
            this.btnMD5Browse.TabIndex = 7;
            this.btnMD5Browse.Text = "Browse";
            this.btnMD5Browse.UseVisualStyleBackColor = true;
            this.btnMD5Browse.Click += new System.EventHandler(this.btnMD5Browse_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnSHABrowse);
            this.groupBox2.Controls.Add(this.txtShaFile);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.txtSHA256Hash);
            this.groupBox2.Location = new System.Drawing.Point(12, 121);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(467, 77);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "SHA256";
            // 
            // btnSHABrowse
            // 
            this.btnSHABrowse.Location = new System.Drawing.Point(386, 17);
            this.btnSHABrowse.Name = "btnSHABrowse";
            this.btnSHABrowse.Size = new System.Drawing.Size(75, 23);
            this.btnSHABrowse.TabIndex = 12;
            this.btnSHABrowse.Text = "Browse";
            this.btnSHABrowse.UseVisualStyleBackColor = true;
            this.btnSHABrowse.Click += new System.EventHandler(this.btnSHABrowse_Click);
            // 
            // txtShaFile
            // 
            this.txtShaFile.Location = new System.Drawing.Point(69, 19);
            this.txtShaFile.Name = "txtShaFile";
            this.txtShaFile.ReadOnly = true;
            this.txtShaFile.Size = new System.Drawing.Size(311, 20);
            this.txtShaFile.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(28, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(26, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "File:";
            // 
            // txtSHA256Hash
            // 
            this.txtSHA256Hash.Location = new System.Drawing.Point(69, 45);
            this.txtSHA256Hash.Name = "txtSHA256Hash";
            this.txtSHA256Hash.ReadOnly = true;
            this.txtSHA256Hash.Size = new System.Drawing.Size(392, 20);
            this.txtSHA256Hash.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(28, 48);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Hash:";
            // 
            // HashForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(491, 209);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "HashForm";
            this.Text = "HashForm";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnMD5;
        private System.Windows.Forms.TextBox txtMD5Hash;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtText;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnMD5Browse;
        private System.Windows.Forms.TextBox txtMD5File;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnSHABrowse;
        private System.Windows.Forms.TextBox txtShaFile;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtSHA256Hash;
    }
}