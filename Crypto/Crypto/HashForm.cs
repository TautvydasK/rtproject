﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Crypto
{
    public partial class HashForm : Form
    {
        public HashForm()
        {
            InitializeComponent();
        }

        private void btnMD5_Click(object sender, EventArgs e)
        {
            txtMD5Hash.Text = CryptoHelper.MD5String(txtText.Text);
        }

        private void btnMD5Browse_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog
            {
                Title = "Browse to file..."
            };

            if(ofd.ShowDialog() == DialogResult.OK)
            {
                txtMD5Hash.Text = CryptoHelper.MD5File(ofd.FileName);
                txtMD5File.Text = ofd.FileName;
            }


        }

        private void btnSHABrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();

            if(ofd.ShowDialog() == DialogResult.OK)
            {
                txtShaFile.Text = ofd.FileName;
                txtSHA256Hash.Text = CryptoHelper.SHA256File(ofd.FileName);
            }
        }
    }
}
