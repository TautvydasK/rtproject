﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Crypto
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnHash_Click(object sender, EventArgs e)
        {
            (new HashForm()).ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            (new SymetricalForm()).ShowDialog();
        }
    }
}
